//
//  ParentViewController.m
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "ParentViewController.h"

#import "TMActivityVC.h"

@interface ParentViewController () 
{
    TMActivityVC *spinner;
}

@end

@implementation ParentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



#pragma mark PUBLIC METHODS

#pragma mark SPINNER START/STOP METHODS

- (void) showSpinningWithText:(NSString *)text
{
    if (spinner == nil)
    {
        spinner = [[TMActivityVC alloc] initWithFrame:self.view.bounds andText:text];
    }
    
    //Take out the screen the tabBar
    self.tabBarController.tabBar.frame = CGRectMake(0, self.tabBarController.tabBar.frame.origin.y + self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.width, self.tabBarController.tabBar.frame.size.height);
    
    
    [self.view insertSubview:spinner.view aboveSubview:self.view];
}

- (void) hideSpinning
{
    [spinner.view removeFromSuperview];
    
    spinner = nil;
    
    //Take in the screen the tabBar
    self.tabBarController.tabBar.frame = CGRectMake(0, self.tabBarController.tabBar.frame.origin.y - self.tabBarController.tabBar.frame.size.height, self.tabBarController.tabBar.frame.size.width, self.tabBarController.tabBar.frame.size.height);
}



@end
