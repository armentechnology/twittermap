//
//  TMAnnotationView.h
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface TMAnnotationView : MKAnnotationView

@property (nonatomic, assign) NSNumber *distance;
@property (nonatomic, assign) NSString *title;
@property (nonatomic, assign) NSString *subtitle;
@property (nonatomic, assign) CLLocationCoordinate2D coordinates;


@end
