//
//  TMActivityVC.m
//  TwitterMapBetVictor
//
//  Created by Jose Fco. Catala Barba on 07/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "TMActivityVC.h"

@interface TMActivityVC ()
{
    UILabel *activityLabel;
    UIActivityIndicatorView *activityIndicator;
    UIView *container;
    CGRect frame;
}

@end

@implementation TMActivityVC

-(id)initWithFrame:(CGRect) theFrame andText:(NSString *)text {
    if (self = [super init])
    {
        frame = theFrame;
        self.view.frame = theFrame;
        if (text)
        {
            activityLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, frame.size.height / 1.25, frame.size.width, 50)];
            activityLabel.text = text;
            activityLabel.numberOfLines = 0;
            activityLabel.textColor = [UIColor whiteColor];
            activityLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
            activityLabel.textAlignment = NSTextAlignmentCenter;
            activityLabel.backgroundColor = [UIColor blackColor];
            [self.view addSubview:activityLabel];
        }
    }
    return self;
}

-(void)viewWillAppear:(BOOL) animated {
    [super viewWillAppear:animated];
    [activityIndicator startAnimating];
}

-(void)viewWillDisappear:(BOOL) animated
{
    [super viewWillDisappear:animated];
    [activityIndicator stopAnimating];
}

-(void)dealloc
{
    if (container != nil) container = nil;
    if (activityLabel != nil) activityLabel = nil;
    if (activityIndicator != nil) activityIndicator = nil;
}

-(void)loadView
{
    [super loadView];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 100,100)];
    activityIndicator.center = self.view.center;
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicator startAnimating];
    
    [self.view addSubview:activityIndicator];
    
    self.view.backgroundColor = [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:0.5];
}

@end
