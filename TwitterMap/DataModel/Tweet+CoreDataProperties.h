//
//  Tweet+CoreDataProperties.h
//  TwitterMapBetVictor
//
//  Created by Jose Fco. Catala Barba on 07/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tweet.h"

NS_ASSUME_NONNULL_BEGIN


@interface Tweet (CoreDataGeneratedAccessors)

+ (void) insertTweetFromDictionary:(NSDictionary *)dicTweet;

/*!
 *@description Return the array of tweets sorted by distance
 */
+ (NSArray *) fetchTweetsFromLocalData;

+ (NSUInteger) numberOfTweetsInDataBase;

+ (void) insertGeolocatedTweetWithDictionary:(NSDictionary *)dicTweet;

+ (void) deleteOldTweets;


NS_ASSUME_NONNULL_END

@end
