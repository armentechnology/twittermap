//
//  Tweet+CoreDataProperties.m
//  TwitterMapBetVictor
//
//  Created by Jose Fco. Catala Barba on 07/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "Tweet+CoreDataProperties.h"
#import <CoreLocation/CoreLocation.h>
#import "TMMapView.h"
#import "TMConstants.h"

@implementation Tweet (CoreDataProperties)


+ (void) insertTweetFromDictionary:(NSDictionary *)dicTweet
{
    //This looks like a bad backend implementation in tweeter api, the coordinates array with duplicated key and strings for float values
    NSArray *coordinates = [dicTweet objectForKey:@"coordinates"];
    
    if (coordinates == (id) [NSNull null])
    {
        //It is quite difficult to catch tweets geolocated, so I´m getting the coordinates from user => location string using geocode services instead
        
        NSString *stringForGeoCode = [[dicTweet objectForKey:@"user"] objectForKey:@"location"];
        
        if (stringForGeoCode.length < 5)
        {
            return;
        }
        
        CLGeocoder *geocoder = [[CLGeocoder alloc]init];
        
        __weak __typeof__(self) weakSelf = self;
        
        [geocoder geocodeAddressString:stringForGeoCode completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
            if (error == nil && [placemarks count] > 0)
            {
                [weakSelf geocodeDidFinishWithPlaceMarks:placemarks forTweetDic:dicTweet];
            }
            
            return;
        }];

    }
    else
    {
        [Tweet insertGeolocatedTweetWithDictionary:dicTweet];
    }
}

+ (void) geocodeDidFinishWithPlaceMarks:(NSArray *)placemarks forTweetDic:(NSDictionary *)dicTweet
{
    CLPlacemark *placemark = [placemarks firstObject];
    
    float lat = placemark.location.coordinate.latitude;
    float lng = placemark.location.coordinate.longitude;
    
    //The coordinates with the tweets are coming as strings, so we need the same format here and rebuild the original coordinates key
    NSDictionary *coordinates = @{@"coordinates":@[[NSString stringWithFormat:@"%0.8f",lat] ,[NSString stringWithFormat:@"%0.8f",lng]]};
    
    NSMutableDictionary *tmpTweetDic = [[NSMutableDictionary alloc]initWithDictionary:dicTweet];
    
    [tmpTweetDic setObject:coordinates forKey:@"coordinates"];
    
    [Tweet insertGeolocatedTweetWithDictionary:tmpTweetDic];
}

+ (NSArray *) fetchTweetsFromLocalData
{
    id delegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Tweets"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distanceTweet"
                                                                   ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil) {
        
        //Deal with failure
    }
    else {
        
        //Deal with success
        return results;
    }
    
    return @[];
}

+ (NSUInteger) numberOfTweetsInDataBase
{
    id delegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Tweets" inManagedObjectContext:managedObjectContext]];
    
    [request setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    NSError *err;
    NSUInteger count = [managedObjectContext countForFetchRequest:request error:&err];
    if(err) {
        //Handle error
    }
    
    return count;
}

+ (void) deleteOldTweets
{
    id delegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Tweets" inManagedObjectContext:managedObjectContext]];
    
    //The logic to rest the time a Tweet has to be push out
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    NSDate *now = [[NSDate alloc] init];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setSecond:-kTMLifespan];
    NSDate *fireTime = [calendar dateByAddingComponents:offsetComponents toDate:now options:0];
    //end of time logic
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateInserted < %@", fireTime];
    
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    //error handling goes here
    for (NSManagedObject *object in objects) {
        [managedObjectContext deleteObject:object];
    }
    
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];

}

#pragma mark PRIVATE METHODS

+ (void) insertGeolocatedTweetWithDictionary:(NSDictionary *)dicTweet
{
    NSString *tweetId = [dicTweet objectForKey:@"id_str"];
    NSString *textTweet = [dicTweet objectForKey:@"text"];
    NSString *pictureUser = [[dicTweet objectForKey:@"user"] objectForKey:@"profile_image_url_https"];
    
    //The Core Data is ready to be used, but for now it is for testing purposes
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tweetId == %@", tweetId];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Tweets" inManagedObjectContext:managedObjectContext]];
    [request setPredicate:predicate];
     NSError *error = nil;
     NSArray *obj = [managedObjectContext executeFetchRequest:request error:&error];
    
    if (obj.count > 0)
    {
        //This means that the tweet is already in the data base
        return;
    }
    
    
    Tweet *newTweet = [NSEntityDescription insertNewObjectForEntityForName:@"Tweets"
                                                    inManagedObjectContext:managedObjectContext];
    
    NSArray *coordinates = [[dicTweet objectForKey:@"coordinates"] objectForKey:@"coordinates"];
    
    
    newTweet.tweetId        =  tweetId;
    
    NSArray *hashtags = [[dicTweet objectForKey:@"entities"] objectForKey:@"hashtags"];
    
    if (hashtags.count > 0)
    {
        NSString *titleTweet = [[hashtags objectAtIndex:0] objectForKey:@"text"];
        newTweet.titleTweet     = [NSString stringWithFormat:@"#%@",titleTweet];
    }
    else
    {
        newTweet.titleTweet = @"No #hashtag provided";
    }
    
    
    
    newTweet.textTweet      = textTweet;
    
    
    newTweet.pictureUser    = pictureUser;
    
    newTweet.dateInserted   = [NSDate date];
    
    newTweet.latTweet    = [NSString stringWithFormat:@"%@",[coordinates objectAtIndex:0]];
    newTweet.lngTweet    = [NSString stringWithFormat:@"%@",[coordinates objectAtIndex:1]];
    
    CLLocationCoordinate2D tweetCoordinates = CLLocationCoordinate2DMake([newTweet.latTweet floatValue], [newTweet.lngTweet floatValue]);
    
    //Get the distance using the method in MapView class
    NSNumber *distance = [[[TMMapView alloc] init] returnDistanceForCoordinates:tweetCoordinates];
    
    float miles = [distance floatValue] * 0.000621371;
    
    [newTweet setDistanceTweet:[NSString stringWithFormat:@"%0.0f",miles]];
    
    // Save the context only if we have a valid coordinates
    if (![managedObjectContext save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

}


@end
