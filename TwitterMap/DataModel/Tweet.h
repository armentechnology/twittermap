//
//  Tweet.h
//  TwitterMapBetVictor
//
//  Created by Jose Fco. Catala Barba on 07/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Tweet : NSManagedObject

@property  NSString *tweetId;
@property  NSString *titleTweet;
@property  NSString *textTweet;
@property  NSString *pictureUser;
@property  NSString *latTweet;
@property  NSString *lngTweet;
@property  NSString *distanceTweet;
@property  NSDate *dateInserted;

/*
*
* NOT IMPLEMENTED THE WHOLE MODEL, ONLY A SAMPLE
*
*/
@end

