//
//  Tweet.m
//  TwitterMapBetVictor
//
//  Created by Jose Fco. Catala Barba on 07/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "Tweet.h"

@implementation Tweet

@dynamic tweetId;
@dynamic titleTweet;
@dynamic textTweet;
@dynamic pictureUser;
@dynamic latTweet;
@dynamic lngTweet;
@dynamic distanceTweet;
@dynamic dateInserted;

@end
