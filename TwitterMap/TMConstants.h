//
//  TMConstants.h
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#ifndef TMConstants_h
#define TMConstants_h


///Number in seconds to remove tweets from the database
static NSInteger kTMLifespan = 500;

///Number in seconds to remove tweets from the database, a low value here can cause a ban from twitter
static NSInteger kTMTimerTimeCheck = 120;

///Number of minimun tweets in data base, the api will be called until this number is reached
static NSInteger kTMMaxNumberOfTweets = 10;

static NSString *kTMNewTweetsNotification = @"TMNewTweetsNotification";

//Twitter stuff
static NSString* kTMConsumerKey = @"CtyeZg0p4lGv9qPCRA83c6giC";

static NSString* kTMConsumerSecretKey = @"ldSdKkd5nJOTvpbJ0CasIDNQFry8HMQD3uCNb1F2P9PgQE2sB5";

static NSString* kTMTwitterAuthAPI = @"https://api.twitter.com/oauth2/token";

static NSString *kTMTwitterAccessURL = @"https://api.twitter.com/oauth2/token";

static NSString *kTMBasicSearURL = @"https://api.twitter.com/1.1/search/tweets.json";

static NSString *kTMTwitterSearchURL = @"https://api.twitter.com/1.1/search/tweets.json?q=me";

#endif /* TMConstants_h */
