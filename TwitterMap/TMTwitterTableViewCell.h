//
//  TMTwitterTableViewCell.h
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMTwitterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgTweet;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;

@end
