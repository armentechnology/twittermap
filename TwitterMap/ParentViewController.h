//
//  ParentViewController.h
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMConstants.h"

@interface ParentViewController : UIViewController


- (void) showSpinningWithText:(NSString *)text;

- (void) hideSpinning;


@end
