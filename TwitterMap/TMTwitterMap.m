//
//  TMTwitterMap.m
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "TMTwitterMap.h"
#import "TMMapViewController.h"
#import "Tweet+CoreDataProperties.h"
#import "TMParser.h"



@interface TMTwitterMap () 
{
    __weak IBOutlet UIView *viewMapContainer;
    
}

@property (strong, nonatomic)  TMMapViewController *myMap;

@end

@implementation TMTwitterMap

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Add the map to the view
    _myMap = [[TMMapViewController alloc]initWithNibName:@"TMMapViewController" bundle:nil];
    
    viewMapContainer = _myMap.view;
        
    _myMap.view.frame = self.view.frame;
    
    [self.view addSubview: _myMap.view];
    
    [NSTimer scheduledTimerWithTimeInterval:kTMTimerTimeCheck target:self selector:@selector(updateTweetsDataBase:) userInfo:nil repeats:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)updateTweetsDataBase:(NSTimer *)sender
{
    //Dont delete or try to reload data if there is not an internet connection available
    if (![[TMParser sharedInstance] isInternetAvailable])
    {
        return;
    }
    
    //Check to delete old tweets
    [Tweet deleteOldTweets];
    
    //Download new tweets, the map will be updatec automatically after the fetching process
    [_myMap fetchDataFromAPI];
}





@end
