//
//  TMMapView.m
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "TMMapView.h"

@implementation TMMapView


- (NSNumber *) returnDistanceForCoordinates:(CLLocationCoordinate2D)coordinate
{
    CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:self.userLocation.coordinate.latitude longitude:self.userLocation.coordinate.longitude];
    CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    CLLocationDistance distance = [startLocation distanceFromLocation:endLocation];
    
    NSNumber *rtnDistance = [NSNumber numberWithFloat:distance];
    
    return rtnDistance;
}
@end
