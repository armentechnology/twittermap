//
//  TMActivityVC.h
//  TwitterMapBetVictor
//
//  Created by Jose Fco. Catala Barba on 07/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMActivityVC : UIViewController



-(id)initWithFrame:(CGRect) theFrame andText:(NSString *)text;

@end
