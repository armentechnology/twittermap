//
//  TMMapView.h
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface TMMapView : MKMapView


/*!
 *@description Return the distance between the user location and another coordinate
 *@param coordinate The coordinate in CLLocationCoordinate2D format
 */
- (NSNumber *) returnDistanceForCoordinates:(CLLocationCoordinate2D)coordinate;

@end
