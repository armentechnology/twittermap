//
//  TMParser.h
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>



@protocol TMParserDelegate <NSObject>

@optional

- (void) receiveJsonResponse:(NSDictionary *)responseDict withSuccess:(BOOL)successBool;


@end

@interface TMParser : NSObject

@property (weak, nonatomic) id <TMParserDelegate>delegate;

+ (TMParser*) sharedInstance;

/*!
 *@description Return the access token needed to some twitter calls
 */
- (void)verifyCredentialsAndGetAccessToken:(void(^)(NSString *accessToken, NSError *error))block;

/*!
 *@description Make a call using the token passed as parameter. The hostName parameter has to be a valid url
 */
- (void) parseJsonUsingGetOnHost:(NSString*)hostName  andToken:(NSString *)token;


- (BOOL) isInternetAvailable;

@end
