//
//  TMParser.m
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "TMParser.h"
#import "TMConstants.h"
#import "Tweet+CoreDataProperties.h"
#import "Reachability.h"

static int timeOutInt = 10;

static TMParser *parserInstance = nil;

@interface TMParser () <NSURLSessionDataDelegate, TMParserDelegate>
{
    // Common Parameters
    NSMutableData* webData;
    
    NSURLSession *theConnection;
    NSURLSessionDataTask *sessionTask;
}


@end

@implementation TMParser
{
    NSMutableArray *geoTaggedTweets;
    
    NSString *accessToken;
    
    NSInteger fetchedTweets;
    
}


+ (TMParser *) sharedInstance
{
    @synchronized(self)
    {
        if (parserInstance == nil)
        {
            parserInstance = [[TMParser alloc] init];
        }
    }
    return parserInstance;
}

- (id) init
{
    self = [super init];
    if (self)
    {
        self.delegate = self;
        geoTaggedTweets = [[NSMutableArray alloc]init];
        fetchedTweets = 0;
    }
    return self;
}

- (void) parseJsonUsingGetOnHost:(NSString*)hostName andToken:(NSString *)token
{
    accessToken = token;
    
    NSURL* finalUrl = [NSURL URLWithString:hostName];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:finalUrl
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:timeOutInt];
    
    NSString *authValue = [NSString stringWithFormat:@"Bearer %@",token];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    theConnection = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]
                                                  delegate:self
                                             delegateQueue:[NSOperationQueue mainQueue]];
    
    sessionTask = [theConnection dataTaskWithRequest:request];
    
    [sessionTask resume];
    
    if (sessionTask)
    {
        webData = [NSMutableData data];
        
    }else
    {
        [_delegate receiveJsonResponse:NULL withSuccess:NO];
    }
}


- (void)verifyCredentialsAndGetAccessToken:(void(^)(NSString *accessToken, NSError *error))block{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kTMTwitterAuthAPI]
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:timeOutInt];
    [request setHTTPMethod:@"POST"];
    NSString *str = @"grant_type=client_credentials";
    NSData *httpBody = [str dataUsingEncoding:NSUTF8StringEncoding];
    
    [request setHTTPBody:httpBody];
    
    NSString *contentLength = [NSString stringWithFormat:@"%lu",(unsigned long)httpBody.length];
    [request setValue:contentLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    NSString *b64string = [self getBase64EncodedBearerToken];
    [request setValue:[NSString stringWithFormat:@"Basic %@", b64string] forHTTPHeaderField:@"Authorization"];
    
    NSLog(@"Request: %@",request);
    
    [self performURLSessionTaskForRequest:request successBlock:^(id responseObject)
     {
        // Your access_token is here.
        if(responseObject && [responseObject isKindOfClass:[NSDictionary class]])
        {
            if(responseObject[@"access_token"])
            {
                NSString *token = responseObject[@"access_token"];
                if(block)
                    block(token, nil);
            } else
            {
                if(block)
                    block(nil, nil);
            }
        }
    }
    errorBlock:^(NSError *error)
    {
        if (block)
            block(nil, error);
    }];
}

#pragma mark NSURLSESION DELEGATE

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response  completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler
{
    completionHandler(NSURLSessionResponseAllow);
    
    [webData setLength:0];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    [webData appendData:data];
}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error)
    {
        NSDictionary *errorDic = @{@"error":error};
        
        [_delegate receiveJsonResponse:errorDic withSuccess:NO];
    }
    else
    {
        //NSString *dataString = [[NSString alloc]initWithData:webData encoding:NSUTF8StringEncoding];
        //NSLog(@"Data from server: %@",dataString);
        
        NSError *error;
        
        id obj = [NSJSONSerialization JSONObjectWithData:webData  options:kNilOptions error:&error];
        
        if (!error)
        {
            NSDictionary *rDic = [NSDictionary new];
            
            if ([obj isKindOfClass:[NSDictionary class]])
            {
                rDic = (NSDictionary *)obj;
                
                NSArray *statuses = [rDic objectForKey:@"statuses"];
                
                if (statuses.count > 0)
                {
                    for (NSDictionary *tweetDic in statuses)
                    {                        
                        [Tweet insertTweetFromDictionary:tweetDic];
                    }
                    
                    NSUInteger numberOfTweets = [Tweet numberOfTweetsInDataBase];
                    
                    
                    if (numberOfTweets < kTMMaxNumberOfTweets)
                    {
                        //Make an additional call to Twitter API until we reach 15 valids tweets
                        NSDictionary *searchMetadataDic = [rDic objectForKey:@"search_metadata"];
                        NSString *nextResults = [searchMetadataDic objectForKey:@"next_results"];
                        NSString *newURL = [NSString stringWithFormat:@"%@%@",kTMBasicSearURL,nextResults];
                        [self parseJsonUsingGetOnHost:newURL andToken:accessToken];
                    }
                    else
                    {
                        if ([_delegate respondsToSelector:@selector(receiveJsonResponse:withSuccess:)])
                        {
                            [_delegate receiveJsonResponse:nil withSuccess:YES];
                        }
                    }
                }
                else
                {
                    /*
                      { "errors": [ { "code": 88, "message": "Rate limit exceeded" } ] }
                     */
                    if ([rDic objectForKey:@"errors"])
                    {
                        if ([_delegate respondsToSelector:@selector(receiveJsonResponse:withSuccess:)])
                        {
                            [_delegate receiveJsonResponse:rDic withSuccess:NO];
                        }
                    }
                }
            }
        }
        else
        {
            NSDictionary *errorDic = @{@"error":error,@"data":[[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding]};
            
            if (_delegate)
            {
                 [_delegate receiveJsonResponse:errorDic withSuccess:NO];
            }
        }
    }
}

#pragma mark - Perform URLSession Task

/*
 * Method to perform session task
 */

- (void)performURLSessionTaskForRequest:(NSURLRequest *)request
                           successBlock:(void(^)(id responseObject))successBlock
                             errorBlock:(void(^)(NSError *error))errorBlock{
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error || !data) {
            NSLog(@"Error: %@", error);
            if(errorBlock){
                errorBlock(error);
            }
            return;
        }
        
        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            if (statusCode != 200) {
                NSLog(@"Error: %@", response);
                if(errorBlock){
                    errorBlock(nil);
                }
                return;
            }
        }
        
        if(data){
            NSError *parseError;
            id responseObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            if (!responseObject) {
                NSLog(@"Error in Parsing Data");
                if(errorBlock){
                    errorBlock(parseError);
                }
            } else {
                NSLog(@"ResponseObject for request: %@", request.URL);
                if(successBlock){
                    successBlock(responseObject);
                }
            }
        }
    }];
    [task resume];
}


#pragma mark - Private Methods

/*
 * percent escape legal characters
 * replace " " with "+" for query params
 */

- (NSString *)percentEscapeString:(NSString *)string
{
    string = [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return [string stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}


/*
 * BearerToken = "ConsumerKey" + ":" + "ConsumerSecretKey"
 */

- (NSString *)getBase64EncodedBearerToken
{
    NSString *encodedConsumerToken = [self percentEscapeString:kTMConsumerKey];
    NSString *encodedConsumerSecret = [self percentEscapeString:kTMConsumerSecretKey];
    NSString *bearerTokenCredentials = [NSString stringWithFormat:@"%@:%@", encodedConsumerToken, encodedConsumerSecret];
    NSData *data = [bearerTokenCredentials dataUsingEncoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
}

// CHECK FOR INTERNET CONNECTION AVAILABLE

- (BOOL) isInternetAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
