//
//  TMTwitterTable.m
//  TwitterMap
//
//  Created by Jose Fco. Catala Barba on 06/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "TMTwitterTable.h"
#import "TMTwitterTableViewCell.h"
#import "Tweet+CoreDataProperties.h"

@interface TMTwitterTable ()
{
    NSArray *dataToShow;
    
    NSMutableDictionary *imagesStore;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TMTwitterTable

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    dataToShow = [Tweet fetchTweetsFromLocalData];
    
    imagesStore = [[NSMutableDictionary alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTweets:) name:kTMNewTweetsNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataToShow.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (cell == nil)
    {
        cell = [self returnListCellForIndex:indexPath];
    }
    
    return cell;
}

- (TMTwitterTableViewCell *) returnListCellForIndex:(NSIndexPath *)index
{
    static NSString *listCellIdentifier = @"Cell";
    
    TMTwitterTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:listCellIdentifier forIndexPath:index];
    
    Tweet *tweet = [[Tweet alloc]init];
    
    tweet = [dataToShow objectAtIndex:index.row];
    
    cell.lblText.text = tweet.textTweet;
    cell.lblTitle.text = tweet.titleTweet;
    cell.lblDistance.text = [NSString stringWithFormat:@"%@ miles",tweet.distanceTweet];
    
    //Use an image store to avoid reloading images on table scroll
    NSString *key = [NSString stringWithFormat:@"%@",tweet.tweetId];
    
    if ([imagesStore objectForKey:key])
    {
        cell.imgTweet.image = [imagesStore objectForKey:key];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(0, 0),^{
            
            NSURL *url = [NSURL URLWithString:tweet.pictureUser];
            
            NSData *imgData = [NSData dataWithContentsOfURL:url];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIImage *image = [UIImage imageWithData:imgData];
                
                // Begin a new image that will be the new image with the rounded corners
                // (here with the size of an UIImageView)
                UIGraphicsBeginImageContextWithOptions(cell.imgTweet.bounds.size, NO, 1.0);
                
                // Add a clip before drawing anything, in the shape of an rounded rect
                [[UIBezierPath bezierPathWithRoundedRect:cell.imgTweet.bounds
                                            cornerRadius:cell.imgTweet.frame.size.width / 2] addClip];
                // Draw your image
                [image drawInRect:cell.imgTweet.bounds];
                
                // Get the image, here setting the UIImageView image
                image = UIGraphicsGetImageFromCurrentImageContext();
                
                cell.imgTweet.image = image;
                
                [imagesStore setObject:image forKey:tweet.tweetId];
                // Lets forget about that we were drawing
                UIGraphicsEndImageContext();
            });
        });
    }
    
    return cell;
}

#pragma mark TABLEVIEW DELEGATE

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ///Change the cell height depending on the screen size
    return ceil(self.tableView.frame.size.height / 5);
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL) tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  YES;
}

#pragma mark PRIVATE METHODS

- (void) updateTweets:(NSNotification *)sender
{
    dataToShow = [Tweet fetchTweetsFromLocalData];

    [self.tableView reloadData];
}

@end
