//
//  TMMapViewController.m
//  TwitterMapBetVictor
//
//  Created by Jose Fco. Catala Barba on 07/01/17.
//  Copyright © 2017 Armentechnology S.L. All rights reserved.
//

#import "TMMapViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "TMMapView.h"
#import "TMAnnotationView.h"
#import "TMCustomAnnotation.h"
#import "Tweet+CoreDataProperties.h"
#import "TMParser.h"
#import "TMConstants.h"

@interface TMMapViewController () <MKMapViewDelegate, CLLocationManagerDelegate, TMParserDelegate>
{
    TMParser *parser;
    
    CLLocationManager *locationManager;
    
    NSString *tokenSaved;
}

@property (weak, nonatomic) IBOutlet TMMapView *myMap;
@property (strong, nonatomic) UIView *myCalloutView;

@end

@implementation TMMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _myMap.delegate = self;
    _myMap.showsUserLocation = YES;
    _myMap.mapType = MKMapTypeStandard;
    
    locationManager = [[CLLocationManager alloc]init];
    
    if (! [[self parser] isInternetAvailable])
    {
        [self showAlertWithMessage:@"Please, check your internet connection" andTitle:@"Warning"];
        
        //Load the old pins until an internet connection is available
        [self loadTweetPins];
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [locationManager requestWhenInUseAuthorization];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark LAZY CODE

- (TMParser *)parser
{
    if (!parser)
    {
        parser = [[TMParser alloc]init];
        parser.delegate = self;
    }
    return parser;
}

#pragma mark MAPVIEW  ANNOTATIONS DELEGATE

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //Fetch new data when the user location change if internet connection
    if ([[self parser] isInternetAvailable])
    {
        [self fetchDataFromAPI];
    }
    else
    {
        [self showAlertWithMessage:@"Check your internet connection" andTitle:@"Notice!"];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if (mapView.userLocation == annotation) {
        return nil;
    }
    
    static NSString *const UCReuseID = @"UCReuseID";
    
    TMAnnotationView *annotationView = (TMAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:UCReuseID];
    
    if (!annotationView)
    {
        annotationView = [[TMAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:UCReuseID];
    }
    
    //annotationView.propertyId = ......
    
    //There is a view ready to be customized in case we need a custom callout
    annotationView.canShowCallout = NO;
    
    annotationView.title = [(TMCustomAnnotation *)annotation title];
    
    annotationView.coordinates = [(TMCustomAnnotation *)annotation coordinate];
    
    annotationView.image = [UIImage imageNamed:@"twitterPin"];
    
    //By default the center of the image is pointing the coordinates, need to correct this
    annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height / 2);
    
    return annotationView;
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(nonnull MKAnnotationView *)view
{
    [_myCalloutView removeFromSuperview];
    
    _myCalloutView = nil;
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    //Dont show the callout view if the user taps on its own pin
    
     if ([view.annotation isKindOfClass:[MKUserLocation class]])
     {
     [mapView deselectAnnotation:view.annotation animated:YES];
     
     return;
     }
    
    TMCustomAnnotation *annotation = view.annotation;
    
    CLLocationCoordinate2D coordinate = annotation.coordinate;
    
    //Center the callout and put it at the bottom of the pin
    UIViewController *uv = [[UIViewController alloc]initWithNibName:@"TMMyCalloutView" bundle:nil];
    uv.view.frame = CGRectMake(0, 0, 200, 100);
    _myCalloutView = [uv view];
    
    UILabel *lbl = [_myCalloutView viewWithTag:100]; //This is the tag for the label
    
    lbl.text = annotation.title;
    
    //Just in case we need the access to the Tweet object in the subtitle is stored the id
    _myCalloutView.restorationIdentifier = annotation.subtitle;
    
    _myCalloutView.center = CGPointMake(view.frame.size.width / 2, - _myCalloutView.frame.size.height / 2);
    
    [view  insertSubview:_myCalloutView atIndex:0];
    
    [_myMap setCenterCoordinate:coordinate animated:YES];
}

#pragma mark PUBLIC METHODS

- (void) loadTweetPins
{
    NSMutableArray *auxArray = [[NSMutableArray alloc]init];
    
    //We dont need the user location annotation
    for (id obj in _myMap.annotations)
    {
        if (![obj isKindOfClass:[MKUserLocation class]])
        {
            [auxArray addObject:obj];
        }
    }
    
    NSArray *pinsArray = [Tweet fetchTweetsFromLocalData];
    
    for (Tweet *tweet in pinsArray)
    {
        TMCustomAnnotation *annotationPoint = [self returnAnnotationFromTweet:tweet];
       
        if (![auxArray containsObject:annotationPoint])
        {
            [_myMap addAnnotation:annotationPoint];
        }

        //Delete pins are not in the new array
        for (TMCustomAnnotation *oldTweet in auxArray)
        {
            if (![pinsArray containsObject:oldTweet])
            {
                [_myMap removeAnnotation:oldTweet];
            }
        }
    }
}

- (TMCustomAnnotation *) returnAnnotationFromTweet:(Tweet *)sender
{
    CLLocationCoordinate2D annotationCoord;
    
    TMCustomAnnotation *annotationPoint = [[TMCustomAnnotation alloc] init];
    annotationCoord.latitude = [sender.latTweet doubleValue];
    annotationCoord.longitude = [sender.lngTweet doubleValue];
    
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = sender.textTweet;
    annotationPoint.subtitle = sender.tweetId;
    annotationPoint.distance = [_myMap returnDistanceForCoordinates:annotationPoint.coordinate];
    return annotationPoint;
}

- (void) fetchDataFromAPI
{
    __weak __typeof__(self) weakSelf = self;
    
    [[self parser] verifyCredentialsAndGetAccessToken:^(NSString *accessToken, NSError *error)
     {
         if (!error)
         {
             [weakSelf verifyCredentialsAndGetAccessTokenDidFinishWithToken:accessToken];
         }
         else
         {
             NSLog(@"Error in block: %@",error);
         }
     }];
}

- (void) verifyCredentialsAndGetAccessTokenDidFinishWithToken:(NSString *)accessToken
{
    tokenSaved = accessToken;
    
    [[self parser] parseJsonUsingGetOnHost:kTMTwitterSearchURL andToken:accessToken];
}

#pragma ALERT CONTROLLER

- (void) showAlertWithMessage:(NSString *)msg andTitle:(NSString *)title
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
        [alert dismissViewControllerAnimated:YES completion:nil];
            }];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark TMPARSER DELEGATE

- (void) receiveJsonResponse:(NSDictionary *)responseDict withSuccess:(BOOL)successBool
{
    if (successBool)
    {
        //Data Received Ok
        [self loadTweetPins];
        
        //Post a notification for Map and Table list Classes
        [[NSNotificationCenter defaultCenter] postNotificationName:kTMNewTweetsNotification object:nil];
    }
    else
    {
        //If there is an error is because twitter api limit, careful or the app could be banned
        if (![responseDict objectForKey:@"errors"])
        {
            //Search again
            //This should not be implemented, but for now is the only way to take a minimun of geotaged tweets
            [[self parser] parseJsonUsingGetOnHost:kTMTwitterSearchURL andToken:tokenSaved];
        }
    }
}

@end
