# **MAP TWITTER BETVICTOR APP RELEASES NOTES**#

## Fetching data from Twitter API##

Since API ver. 1.1 there are not accessible endpoints without a minimal authentication, so the process followed here is described in Twitter API development guide.
The logic for this is in TMParser.h/m

## Manipulating the data ##

I´m not sure about the number, but probably less than 1% of the twitters have been geolocated. To avoid this problem and feed the app, I started using the geolocating services using as parameter the text found in statuses=>user=>location.
The logic for this is in Tweet+CoreDataProperties.h/m

To check the internet connexion I´m using Reachability.h/m apple class. This is the only external class used in the project.

All the business for the map is a external ViewController, so in case of an app upgrade and the need of to use a map again this one is fully reusable.
The callout and the annotation have been customised, so in future changes or new features (show a picture in the callout, etc.), the job to be done is minimal.

## The Table View##

The loading process for the pictures of the row is detached from the main thread, and the images are stored in memory (NSDictionary), to avoid the app download the same image twice.
A custom tableview cell has been used here so the changes can be easily implemented.

A constant .h file is containing the static constants used by the app. Here we can change the values for refreshing the tweets and for the lifespan

The url to clone the project

https://JFCaBa@bitbucket.org/JFCaBa/twittermap.git


The branches master and dev are now containing the same code.